package com.ericho.listviewcheckbox;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends Activity {

    Button show;
    ListView lv;
    List<Person> persons = new ArrayList<>();
    Context mContext;
    MyListAdapter adapter;
    List<Integer> listItemID = new ArrayList<>();

    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        mContext = getApplicationContext();
        show = (Button) findViewById(R.id.show);
        lv = (ListView) findViewById(R.id.lvperson);

        initPersonData();
        adapter = new MyListAdapter(this, persons);
        lv.setAdapter(adapter);

        show.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                listItemID.clear();
                for (int i = 0; i < adapter.mChecked.size(); i++) {
                    if (adapter.mChecked.get(i)) {
                        listItemID.add(i);
                    }
                }

                if (listItemID.size() == 0) {
                    AlertDialog.Builder builder1 = new AlertDialog.Builder(MainActivity.this);
                    builder1.setMessage("没有选中任何记录");
                    builder1.show();
                } else {
                    StringBuilder sb = new StringBuilder();

                    for (int i = 0; i < listItemID.size(); i++) {
                        sb.append("ItemID=");
                        sb.append(listItemID.get(i));
                        sb.append(" . ");
                    }
                    AlertDialog.Builder builder2 = new AlertDialog.Builder(MainActivity.this);
                    builder2.setMessage(sb.toString());
                    builder2.show();
                }
            }
        });
    }

    /**
     * 模拟数据
     */
    private void initPersonData() {
        Person mPerson;
        for (int i = 1; i <= 30; i++) {
            mPerson = new Person();
            mPerson.setName("Andy" + i);
            mPerson.setAddress("GuangZhou" + i);
            persons.add(mPerson);
        }
    }


}

