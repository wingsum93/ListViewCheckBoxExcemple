package com.ericho.listviewcheckbox;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by EricH on 20/6/2016.
 */
public class MyListAdapter extends BaseAdapter {
    List<Boolean> mChecked;
    List<Person> listPerson;
    Context mContext;
    HashMap<Integer, View> map = new HashMap<Integer, View>();

    public MyListAdapter(Context c, List<Person> list) {
        listPerson = new ArrayList<Person>();
        listPerson = list;
        mContext = c;
        mChecked = new ArrayList<Boolean>();
        for (int i = 0; i < list.size(); i++) {
            mChecked.add(false);
        }
    }

    @Override
    public int getCount() {
        return listPerson.size();
    }

    @Override
    public Object getItem(int position) {
        return listPerson.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view;
        ViewHolder holder = null;

        if (map.get(position) == null) {
            Log.e("MainActivity", "position1 = " + position);

            LayoutInflater mInflater = (LayoutInflater) mContext
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = mInflater.inflate(R.layout.listitem, null);
            holder = new ViewHolder();
            holder.selected = (CheckBox) view.findViewById(R.id.list_select);
            holder.name = (TextView) view.findViewById(R.id.list_name);
            holder.address = (TextView) view.findViewById(R.id.list_address);
            final int p = position;
            map.put(position, view);
            holder.selected.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    CheckBox cb = (CheckBox) v;
                    mChecked.set(p, cb.isChecked());
                }
            });
            view.setTag(holder);
        } else {
            Log.e("MainActivity", "position2 = " + position);
            view = map.get(position);
            holder = (ViewHolder) view.getTag();
        }

        holder.selected.setChecked(mChecked.get(position));
        holder.name.setText(listPerson.get(position).getName());
        holder.address.setText(listPerson.get(position).getAddress());

        return view;
    }

    static class ViewHolder {
        CheckBox selected;
        TextView name;
        TextView address;
    }

}
